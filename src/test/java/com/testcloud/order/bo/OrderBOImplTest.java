package com.testcloud.order.bo;

import com.testcloud.order.bo.exception.BOException;
import com.testcloud.order.dao.OrderDAO;
import com.testcloud.order.dao.OrderDAOImpl;
import com.testcloud.order.dto.Order;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by guillem on 22/02/16.
 */
public class OrderBOImplTest {

    @Mock OrderDAO dao;
    private OrderBOImpl bo;
    Order order;

    @Before public void setup() {
        MockitoAnnotations.initMocks(this);
        bo = new OrderBOImpl();
        bo.setDao(dao);
        order = new Order();
    }

    @Test public void placeOrder_Should_Create_An_Order() throws SQLException, BOException {
        when(dao.create(order)).thenReturn(new Integer(1));
        assertTrue(bo.placeOrder(order));
        verify(dao).create(order);
    }
}
