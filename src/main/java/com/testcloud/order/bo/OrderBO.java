package com.testcloud.order.bo;

import com.testcloud.order.bo.exception.BOException;
import com.testcloud.order.dto.Order;

public interface OrderBO {

	boolean placeOrder(Order order) throws BOException;

	boolean cancelOrder(int id) throws BOException;

	boolean deleteOrder(int id) throws BOException;

}
